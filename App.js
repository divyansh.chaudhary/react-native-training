import React from 'react';
import { View, StyleSheet, Text } from "react-native"
import AppNavigator from './navigations/AppNavigator';
import { React$Node } from './src/types/AppTypes';

const App = () => {
  return (
    <AppNavigator />
  );
};

const styles = StyleSheet.create({

});

export default App;
