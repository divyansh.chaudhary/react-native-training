import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "app",
  initialState: {
    origin: null,
  },
  reducers: {
    setOrigin: (state, action) => {
      state.origin = action.payload
    }
  }
});

export default appSlice.reducer;