import { useNavigation } from '@react-navigation/core'
import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { DrawerActions } from "@react-navigation/native"
import { icons } from "../constants"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import tw from "tailwind-react-native-classnames"
// screens
import HomeScreen from "../Screens/Home";
import CategoryScreen from '../Screens/Category'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const HomeTabs = () => {
    return (
        <Tabs.Navigator
            screenOptions={{
                tabBarActiveTintColor: "#999",
                tabBarInactiveTintColor: "#000"
            }}
        >
            <Tabs.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="home" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Category"
                component={CategoryScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="format-list-bulleted-square" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Brand"
                component={HomeScreen}
                listeners={{
                    tabPress: (e) => e.preventDefault()
                }}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="cards-outline" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Account"
                component={HomeScreen}
                listeners={{
                    tabPress: (e) => e.preventDefault()
                }}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="account-circle-outline" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="My bag"
                component={HomeScreen}
                listeners={{
                    tabPress: (e) => e.preventDefault()
                }}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="shopping-outline" color={color} size={size} />
                    ),
                }}
            />
        </Tabs.Navigator>
    )
}
const HomeNavigator = () => {
    const navigation = useNavigation()

    return (
        <Stack.Navigator
        screenListener={{
            state: (e) => {
                console.log(e.data)
            }
        }}
        >
            <Stack.Screen
                name="Home"
                component={HomeTabs}
                options={{
                    title: "JUST CLIQ",
                    headerTitleStyle: { fontSize: 25 , fontFamily: "CarmenSans-Thin"},
                    headerLeft: () => (
                        <TouchableOpacity
                            style={{ marginLeft: 20 }}
                            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                            <Image
                                source={icons.menu}
                                resizeMode="contain"
                                style={{
                                    width: 25,
                                    height: 25
                                }}
                            />
                        </TouchableOpacity>
                    ),
                    headerRight: () => (
                        <Image
                            source={icons.search}
                            resizeMode="contain"
                            style={{
                                width: 25,
                                height: 25,
                                marginRight: 20
                            }}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    )
}

export default HomeNavigator

const styles = StyleSheet.create({})
