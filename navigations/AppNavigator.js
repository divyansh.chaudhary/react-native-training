import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import CategoryScreen from '../Screens/Category'
import HomeScreen from '../Screens/Home'

const Drawer = createDrawerNavigator()

const AppNavigator = () => {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name="Home">
                    {(props) => <HomeScreen {...props} />}
                </Drawer.Screen>
                <Drawer.Screen name="Category">
                    {(props) => <CategoryScreen {...props} />}
                </Drawer.Screen>
            </Drawer.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigator