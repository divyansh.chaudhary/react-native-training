import React, { useEffect } from 'react'
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import tw from "tailwind-react-native-classnames"
import NavOptions from '../component/NavOptions'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useNavigation } from '@react-navigation/core';
// import {GOOGLE_MAPS_APIKEY} from "@env"

const HomeScreen = () => {
  return (
    <SafeAreaView >
      <View style={[tw`h-full justify-center items-center`]}>
        <Text style={tw`text-2xl text-black font-bold uppercase`}>home screen</Text>
      </View>
    </SafeAreaView>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
})
