import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'

const CategoryScreen = () => {
    return (
    <View style={[tw`h-full justify-center items-center`]}>
        <Text style={tw`text-2xl text-black font-bold uppercase`}>category screen</Text>
      </View>
    )
}

export default CategoryScreen

const styles = StyleSheet.create({})
