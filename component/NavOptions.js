import { useNavigation } from '@react-navigation/core'
import React from 'react'
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Icon } from 'react-native-elements'
import tw from "tailwind-react-native-classnames"

const options = [
  {
    id: "342",
    title: "Get a ride",
    image: "https://links.papareact.com/3pn",
    screen: "MapScreen"
  },
  {
    id: "554",
    title: "Order food",
    image: "https://links.papareact.com/28w",
    screen: "EatScreen"
  }
]

const NavOptions = () => {

  const navigation = useNavigation()
  
  return (
    <FlatList
      data={options}
      horizontal
      renderItem={({ item }) => (
        <TouchableOpacity style={tw`ml-4`} onPress={()=>navigation.navigate(item.screen)}>
          <View style={tw`w-40 bg-gray-200 pt-4 pr-2 pb-4 pl-4 rounded-xl`}>
            <Image
              style={{ width: 120, height: 120, resizeMode: "contain" }}
              source={{
                uri: item.image
              }}
            />
            <Text style={tw`text-lg mt-3 font-semibold`}>{item.title}</Text>
            <Icon name="arrowright" type="antdesign" color="white" style={tw`bg-black rounded-full w-10 p-2 my-2`} />
          </View>
        </TouchableOpacity>
      )}
      keyExtractor={item => item.id}
    />
  )
}

export default NavOptions

const styles = StyleSheet.create({})
